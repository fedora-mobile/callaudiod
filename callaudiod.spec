%global debug_package %{nil}

Name:       callaudiod
Version:    master
Release:    1%{?dist}
Summary:    Daemon for dealing with audio routing during phone calls

License:        GPLv3+
URL:            https://gitlab.com/mobian1/callaudiod
Source0:        https://gitlab.com/mobian1/callaudiod/-/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  meson

BuildRequires:  pkgconfig(alsa)
BuildRequires:  pkgconfig(gobject-2.0)
BuildRequires:  pkgconfig(gio-unix-2.0)
BuildRequires:  pkgconfig(libpulse)
BuildRequires:  pkgconfig(libpulse-mainloop-glib)


%description
callaudiod is a daemon for dealing with audio routing during phone calls.
It provides a D-Bus interface allowing other programs to:

switch audio profiles
output audio to the speaker or back to its original port
mute the microphone


%package devel
Summary: Development files for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%setup -q -n %{name}-%{version}

%build 
%meson
%meson_build

%install
%meson_install


%files
%{_bindir}/%{name}
%{_bindir}/callaudiocli
%dir %{_includedir}/libcallaudio-0
%{_libdir}/libcallaudio-0.so.0
%{_datadir}/dbus-1/interfaces/org.mobian_project.CallAudio.xml
%{_datadir}/dbus-1/services/org.mobian_project.CallAudio.service

%files devel
%{_includedir}/libcallaudio-0/libcallaudio.h
%{_libdir}/libcallaudio-0.so
%{_libdir}/pkgconfig/libcallaudio-0.pc

%doc README.md
%license COPYING

%changelog
* Fri Nov 20 2020 Torrey Sorensen <sorensentor@tuta.io> - master-1
- Master Testing

* Wed Nov 04 2020 Torrey Sorensen <sorensentor@tuta.io> - 0.0.4-1
- Initial packaging
